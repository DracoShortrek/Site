document.addEventListener("DOMContentLoaded", function () {
  buttonOnMenuMobile = document.querySelector('.button-navigation-menu');
  menu = document.querySelector('.button-navigation-menu');
  menu.addEventListener("click", Menut);
});



function Menut() {
  menuMobile = document.querySelector('#menu-on-button');
  textOnMobile = "X";
  textOffMobile = "МЕНЮ";

  //-------------------------------------------------------------------------//
  window.addEventListener('resize', checkMediaQuery);


  if (menuMobile.classList.contains('menu-on-button_on') == true) {
    setTimeout(function () {
      window.addEventListener('click', MenuShow)
      return;
    }, 200)
  } else {
    menuMobile.classList.remove('menu-on-button_off');
    menuMobile.classList.add('menu-on-button_on');

    buttonOnMenuMobile.childNodes[1].textContent = textOnMobile;

    setTimeout(function () {
      window.addEventListener('click', MenuShow)
      return;
    }, 200)
  }
  return;
}

function MenuShow() {
  if (menuMobile.classList.contains('menu-on-button_on') == true) {
    menuMobile.classList.remove('menu-on-button_on');
    menuMobile.classList.add('menu-on-button_off');
    buttonOnMenuMobile.childNodes[1].textContent = textOffMobile;
    setTimeout(function () {
      menuMobile.classList.remove('menu-on-button_off');
      window.removeEventListener("click", MenuShow)
    }, 200)

  }
}

function checkMediaQuery() {
  if (window.innerWidth > 1279) {
    buttonOnMenuMobile.childNodes[1].textContent = textOffMobile;
    menuMobile.classList.remove('menu-on-button_off');
    menuMobile.classList.remove('menu-on-button_on');
    window.removeEventListener("click", MenuShow);
    window.removeEventListener("click", checkMediaQuery);
  }
}








